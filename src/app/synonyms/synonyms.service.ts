import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SynonymsService {
  private URL = 'http://api.datamuse.com/words';
  constructor(private http: HttpClient) { }
  get(word: string) {
    return this.http.get(`${this.URL}?rel_syn=${word}`);
  }
}
