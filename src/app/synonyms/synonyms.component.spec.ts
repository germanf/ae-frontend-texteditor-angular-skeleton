import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynonymsComponent } from './synonyms.component';
import {HttpClientModule} from "@angular/common/http";
import {SynonymsService} from "./synonyms.service";

describe('SynonymPanelComponent', () => {
  let component: SynonymsComponent;
  let fixture: ComponentFixture<SynonymsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynonymsComponent ],
      imports: [HttpClientModule],
      providers: [SynonymsService],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynonymsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
