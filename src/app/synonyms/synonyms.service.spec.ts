import { TestBed } from '@angular/core/testing';

import { SynonymsService } from './synonyms.service';
import { HttpClientModule } from '@angular/common/http';

describe('SynonymsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [SynonymsService],
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: SynonymsService = TestBed.get(SynonymsService);
    expect(service).toBeTruthy();
  });
});
