import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {SynonymsService} from './synonyms.service';

@Component({
  selector: 'app-synonyms',
  templateUrl: './synonyms.component.html',
  styleUrls: ['./synonyms.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SynonymsComponent {
  @Input()
  set selectedWord(value: string) {
    if (value && value.length) {
      this._word = value;
      this.updateSuggestions();
    }
  }

  get selectedWord(): string {
    return this._word;
  }

  @Output() replaceSynonym = new EventEmitter();
  public synonyms: any;

  private _word: string;

  constructor(private svc: SynonymsService, private cd: ChangeDetectorRef) {
  }

  updateSuggestions() {
    this.svc.get(this.selectedWord).subscribe(value => {
      this.synonyms = value;
      this.cd.detectChanges();
    });
  }

  replaceWithSynonym(word) {
    this.replaceSynonym.emit(word);
  }

}
