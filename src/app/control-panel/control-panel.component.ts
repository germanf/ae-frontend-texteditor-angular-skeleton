import { ChangeDetectionStrategy, Component } from '@angular/core';
import {TextService} from '../text-service/text.service';

export enum TextModification {
  bold = 'bold',
  italic = 'italic',
  underline = 'underline',
  color = 'color',
}

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent {
  constructor(private textService: TextService) {}

  protected Colors: string[] = ["blue", "red", "green", "yellow"];

  toBold() {
    this.textService.modifyText(TextModification.bold);
  }

  toItalics() {
    this.textService.modifyText(TextModification.italic);
  }

  toUnderline() {
    this.textService.modifyText(TextModification.underline);
  }

  applyColor(color: string) {
    this.textService.modifyText(TextModification.color, color);
  }
}
