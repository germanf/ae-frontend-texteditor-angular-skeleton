import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TextService } from '../text-service/text.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FileComponent implements OnInit {
  text$: any;
  public class: string;

  constructor(private textService: TextService, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.text$ = this.textService.getText();
    this.class = this.textService.className;
    this.textService.getMockText().subscribe(value => {
      this.text$ = value;
      this.cd.markForCheck();
    });
  }

  onMouseUp() {
    const selection = document.getSelection();
    this.textService.selectionChanged({
      text: selection.toString(),
      index: selection.anchorOffset,
      baseNode: selection.anchorNode.parentNode,
    });
  }

  onTextInnerChange(input, event) {
    const selection = document.getSelection();
    const anchorOffset = selection.anchorOffset;

    this.textService.updateText(event.target.innerHTML);

    setTimeout(() => {
      const range = selection.getRangeAt(0);
      const textNode = event.target.childNodes[0];

      range.setStart(textNode, anchorOffset);
      range.setEnd(textNode, anchorOffset);
    }, 0);
  }

  updateWithSynonym(event) {
    this.textService.updateText(this.textService.getText().replace(this.textService.getSelection(), event));
  }
}
