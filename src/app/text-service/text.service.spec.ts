/* tslint:disable:no-unused-variable */

import { inject, TestBed } from '@angular/core/testing';
import { TextService } from './text.service';

describe('TextService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TextService]
    });
  });

  it('should ...', inject([TextService], (service: TextService) => {
    expect(service).toBeTruthy();
  }));
  it('should test className', inject([TextService], (service: TextService) => {
    expect(service.className).toBe('file');
  }));
  it('should have initial text', inject([TextService], (service: TextService) => {
    expect(service.getText).toBeDefined();
  }));
  it('should add bold class', inject([TextService], (service: TextService) => {
    service.updateText('my new class');
    service.selectionChanged({
      text: 'my',
      baseNode: { className: 'file' },
      index: 0
    });
    service.modifyText('bold');
    expect(service.getText).toBeTruthy('<span class="bold">my</span> new class');
  }));
  it('should change selection', inject([TextService], (service: TextService) => {
    service.selectionChanged({
      text: 'new Text',
      baseNode: null,
      index: 0
    });
    expect(service.getSelection()).toBe('new Text');
    service.selectionChanged({
      text: 'totally new text',
      baseNode: null,
      index: 0
    });
    expect(service.getSelection()).toBe('totally new text');
  }));
});
