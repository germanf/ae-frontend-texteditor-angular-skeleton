import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface ISelection {
  text: string;
  index: number;
  baseNode: any;
}

@Injectable()
export class TextService {
  public className = 'file';
  protected selection: ISelection;
  protected mockText = new BehaviorSubject('A year ago I was in the audience at a gathering of designers in San Francisco. ' +
    'There were four designers on stage, and two of them worked for me. I was there to support them. ' +
    'The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. ' +
    'What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, ' +
    'that modern design problems were very complex. And we ought to need a license to solve them.');

  getText() {
    return this.mockText.getValue();
  }

  getMockText() {
    return this.mockText;
  }

  getSelection() {
    return this.selection && this.selection.text;
  }

  updateText(value) {
    this.mockText.next(value);
  }

  selectionChanged(selected: ISelection) {
    this.selection = selected;
  }

  modifyText(type: string, value?: string) {
    const text = this.getText();
    const node = this.selection.baseNode;

    const newclass = value ? `${type}-${value}` : type;
    // checking if node that's been selected is the node of file component
    if (!node.className.includes(this.className)) {
      // removing existing class if new one is the same
      if (value) {
        node.className.split(' ')
          .filter((c: string) => (c.indexOf(type) > -1))
          .map((c: string) => node.classList.remove(c));
        node.classList.add(newclass);
      } else if (node.className.includes(type)) {
        node.classList.remove(type);
      } else {
        node.classList.add(type);
      }
    } else {
      const newElement = `<span class="${newclass}">${this.selection.text}</span>`;
      const newText: String = text.replace(new RegExp(`^(.{${this.selection.index}}).{${this.selection.text.length}}`), `$1${newElement}`);
      this.updateText(newText);
    }
  }
}
